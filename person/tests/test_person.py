from django.test import TestCase
from django.urls import reverse
from backend import models as bmodels
from backend.unittest import PersonFixtureMixin, PageElements
from unittest.mock import patch

test_fingerprint1 = "1793D6AB75663E6BF104953A634F4BD1E7AD5568"


class TestPerson(PersonFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestPerson, cls).setUpClass()
        cls.page_elements = PageElements()
        cls.page_elements.add_id("view_person_fd_comment")
        cls.page_elements.add_id("edit_ldap_link")
        # cls.page_elements.add_id("edit_am_link") TODO: turn this into a permission-based element
        cls.page_elements.add_id("edit_bio_link")
        cls.page_elements.add_id("edit_fpr_link")
        cls.page_elements.add_id("audit_log")

        cls.visited = cls.persons.activeam
        cls.visited.fullname = "Test"
        cls.visited.save(audit_author=cls.persons.dam, audit_notes="test")
        cls.fingerprints.create("fpr1", person=cls.persons.activeam, fpr=test_fingerprint1, is_active=True, audit_skip=True)
        cls.visitor = cls.persons.dc
        cls.url = cls.visited.get_absolute_url()

    def compute_wanted_page_elements(self, visit_perms):
        """
        Compute what page elements we want, based on visit_perms
        """
        wanted = []
        if "fd_comments" in visit_perms:
            wanted += ["view_person_fd_comment"]
        if "edit_ldap" in visit_perms:
            wanted += ["edit_ldap_link", "edit_fpr_link"]
        if "edit_fpr" in visit_perms:
            wanted += ["edit_fpr_link"]
        if "edit_bio" in visit_perms:
            wanted += ["edit_bio_link"]
        if "view_person_audit_log" in visit_perms:
            wanted += ["audit_log"]
        return wanted

    def assertPageElements(self, response):
        visit_perms = self.visited.permissions_of(self.visitor)
        wanted = self.compute_wanted_page_elements(visit_perms)
        self.assertContainsElements(response, self.page_elements, *wanted)

    def tryVisitingWithPerms(self, perms):
        client = self.make_test_client(self.visitor)
        with patch.object(bmodels.Person, "permissions_of", return_value=perms):
            response = client.get(self.url)
            self.assertEqual(response.status_code, 200)
            self.assertPageElements(response)

    def test_none(self):
        self.tryVisitingWithPerms(set())

    def test_person(self):
        self.tryVisitingWithPerms(set(["view_person_audit_log", "edit_bio"]))
        self.tryVisitingWithPerms(set(["view_person_audit_log", "edit_bio", "edit_ldap", "edit_fpr"]))

    def test_fd_dam(self):
        self.tryVisitingWithPerms(set(["view_person_audit_log", "edit_bio", "fd_comment"]))
        self.tryVisitingWithPerms(set(["view_person_audit_log", "edit_bio", "edit_ldap", "edit_fpr", "fd_comment"]))


class TestEditMixin(PersonFixtureMixin):
    # All fields that we check, with an original and changed value
    ALL_FIELDS = {
        "username": ("old@debian.org", "new@debian.org"),
        "fullname": ("oldcn oldmn oldsn", "newcn newmn newsn"),
        "is_staff": (False, True),
        "email": ("oldemail@example.org", "newemail@example.org"),
        "bio": ("oldbio", "newbio"),
        "status": ("dd_u", "dc"),
        "fd_comment": ("oldfd", "newfd"),
        "pending": ("oldpending", "newpending"),
    }

    @classmethod
    def setUpClass(cls):
        super(TestEditMixin, cls).setUpClass()
        cls.visited = cls.persons.dam
        cls.visitor = cls.persons.dc

        # All fields that this view is able to edit
        cls.edited_fields = []

        # Set all fields to the original value
        cls.reset_visited()

    @classmethod
    def get_edited(cls):
        return cls.visited

    @classmethod
    def reset_visited(cls):
        for name, (oldval, newval) in list(cls.ALL_FIELDS.items()):
            setattr(cls.get_edited(), name, oldval)
        cls.get_edited().save(audit_skip=True)

    def get_post_data(self):
        return {name: newval for name, (oldval, newval) in list(self.ALL_FIELDS.items())}

    def assertChanged(self):
        self.get_edited().refresh_from_db()
        for name, (oldval, newval) in list(self.ALL_FIELDS.items()):
            if name in self.edited_fields:
                self.assertEqual(getattr(self.get_edited(), name), newval)
            else:
                self.assertEqual(getattr(self.get_edited(), name), oldval)

    def assertNotChanged(self):
        self.visited.refresh_from_db()
        for name, (oldval, newval) in list(self.ALL_FIELDS.items()):
            self.assertEqual(getattr(self.get_edited(), name), oldval)

    def assertSuccess(self):
        client = self.make_test_client(self.visitor)

        response = client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertNotChanged()

        response = client.post(self.url, data=self.get_post_data())
        self.visited.refresh_from_db()
        self.assertRedirectMatches(response, self.visited.get_absolute_url())
        self.assertChanged()

    def assertForbidden(self):
        client = self.make_test_client(self.visitor)

        response = client.get(self.url)
        self.assertPermissionDenied(response)
        self.assertNotChanged()

        response = client.post(self.url, data=self.get_post_data())
        self.assertPermissionDenied(response)
        self.assertNotChanged()


class TestEditLDAP(TestEditMixin, TestCase):
    ALL_FIELDS = {
        "cn": ("oldcn", "newcn"),
        "mn": ("oldmn", "newmn"),
        "sn": ("oldsn", "newsn"),
        "email": ("oldldap@example.org", "newldap@example.org"),
        "uid": ("olduid", "newuid"),
    }

    @classmethod
    def setUpClass(cls):
        super(TestEditLDAP, cls).setUpClass()
        cls.url = reverse("person:edit_ldap", args=[cls.visited.lookup_key])
        cls.edited_fields = ["cn", "mn", "sn", "email", "uid"]

    @classmethod
    def get_edited(cls):
        return cls.visited.ldap_fields

    @patch.object(bmodels.Person, "permissions_of", return_value=["edit_ldap", "edit_fpr"])
    def test_success(self, perms):
        self.assertSuccess()

    @patch.object(bmodels.Person, "permissions_of", return_value=[])
    def test_forbidden(self, perms):
        self.assertForbidden()


class TestEditBio(TestEditMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestEditBio, cls).setUpClass()
        cls.url = reverse("person:edit_bio", args=[cls.visited.lookup_key])
        cls.edited_fields = ["bio", "fullname"]

    @patch.object(bmodels.Person, "permissions_of", return_value=["edit_bio"])
    def test_success(self, perms):
        self.assertSuccess()

    @patch.object(bmodels.Person, "permissions_of", return_value=[])
    def test_forbidden(self, perms):
        self.assertForbidden()


class TestEditEmail(TestEditMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestEditEmail, cls).setUpClass()
        cls.url = reverse("person:edit_email", args=[cls.visited.lookup_key])
        cls.edited_fields = ["email"]

    @patch.object(bmodels.Person, "permissions_of", return_value=["edit_email"])
    def test_success(self, perms):
        self.assertSuccess()

    @patch.object(bmodels.Person, "permissions_of", return_value=[])
    def test_forbidden(self, perms):
        self.assertForbidden()
