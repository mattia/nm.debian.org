from django.urls import path
from django.views.generic import RedirectView
from . import views

urlpatterns = [
    path('', RedirectView.as_view(url="/", permanent=True), name="public_index"),

    path('newnm', views.Newnm.as_view(), name="public_newnm"),
    path('newnm/resend_challenge/<key>/', views.NewnmResendChallenge.as_view(), name="public_newnm_resend_challenge"),
    path('newnm/confirm/<nonce>/', views.NewnmConfirm.as_view(), name="public_newnm_confirm"),
    path('managers/', views.Managers.as_view(), name="managers"),
    path('people/', views.People.as_view(), name="people"),
    path('people/<status>/', views.People.as_view(), name="people_by_status"),
    path('stats/', views.Stats.as_view(), name="public_stats"),
    path('stats/graph/', views.StatsGraph.as_view(), name="public_stats_graph"),
    path('findperson/', views.Findperson.as_view(), name="public_findperson"),
    path('audit_log/', views.AuditLog.as_view(), name="public_audit_log"),
]
