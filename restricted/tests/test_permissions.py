from __future__ import annotations
from django.test import TestCase
from django.urls import reverse
from backend.unittest import PersonFixtureMixin
import json


class TestPermissions(PersonFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        for visitor in None, "pending", "dc", "dc_ga", "dm", "dm_ga", "dd_e", "dd_r":
            cls._add_method(cls._test_export_fail, visitor)

        for visitor in "dd_nu", "dd_u", "activeam", "oldam", "fd", "dam":
            cls._add_method(cls._test_export_success, visitor)

    def _test_export_success(self, visitor):
        self.persons.dc.email = "private@example.org"

        client = self.make_test_client(visitor)
        response = client.get(reverse("restricted_db_export"))
        self.assertEqual(response.status_code, 200)
        decoded = json.loads(response.content.decode())
        self.assertIsInstance(decoded, dict)

        for person in decoded["people"]:
            if person["ldap_fields"]["uid"] == "dc":
                self.assertEqual(person["email"], "dc@example.org")
                break
        else:
            self.fail("person 'dc' not found in exported data")

    def _test_export_fail(self, visitor):
        client = self.make_test_client(visitor)
        response = client.get(reverse("restricted_db_export"))
        self.assertPermissionDenied(response)
