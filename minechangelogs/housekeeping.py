import django_housekeeping as hk
from minechangelogs import models as mmodels
import logging

log = logging.getLogger(__name__)


class IndexChangelogs(hk.Task):
    """
    Update minechangelogs index
    """
    def run_main(self, stage):
        indexer = mmodels.Indexer()
        with mmodels.parse_projectb() as changes:
            indexer.index(changes)
        indexer.flush()
