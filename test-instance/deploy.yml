#!/usr/bin/env ansible-playbook
#
# Usage: 
#
# ./deploy.yml -i hosts -l $HOSTNAME
#
- name: Set up a test instance of nm.debian.org
  hosts: all
  become: yes
  vars:
     nm2_root: /srv/nm.debian.org/nm2
  roles:
   - role: apache
     sites:
      - domain: nm-test.debian.net
        root: "{{nm2_root}}/htdocs"
        admin: enrico@debian.org
        wsgi: "{{nm2_root}}/nm2/wsgi.py"
  tasks:
   - name: Enable backports
     copy:
        owner: root
        group: root
        mode: 0644
        dest: /etc/apt/sources.list.d/debian-buster-backports.list
        content: |
           deb https://cdn-aws.deb.debian.org/debian/ buster-backports main contrib

   - name: Update after enabling backports
     apt:
        update_cache: yes

   - name: Install nm.debian.org dependencies
     apt:
        name:
         - gettext
         - python3
         - python3-markdown
         - python3-ldap3
         - python3-psycopg2
         - python3-xapian
         - python3-django
         - python3-django-housekeeping
         - python3-debian
         - python3-debiancontributors
         - python3-djangorestframework
         - python3-requests-oauthlib
         - gnupg
         - debian-keyring
         - python3-git
         - libjs-bootstrap4
         - fonts-fork-awesome
         - libjs-jquery-datatables
         - libjs-jquery-flot
         - python3-model-mommy
         - git
        state: present
        update_cache: no
        default_release: buster-backports

   - name: Create deploy group
     group:
       name: nm
       state: present
       system: yes

   - name: Create deploy user
     user:
       name: nm-web
       groups: nm
       system: yes

   - name: Create deploy directory
     file:
       path: "{{nm2_root}}"
       state: directory

   - name: Clone and update the git repository
     git:
      dest: "{{nm2_root}}"
      repo: https://salsa.debian.org/nm-team/nm.debian.org.git
      version: master

   - name: Configure nm.debian.org
     template:
      dest: "{{nm2_root}}/nm2/local_settings.py"
      src: local_settings.py
      owner: root
      group: nm
      mode: '0640'

   - name: run migrations
     command: "{{nm2_root}}/manage.py migrate"
     args:
      chdir: "{{nm2_root}}"
   - name: static files
     command: "{{nm2_root}}/manage.py collectstatic --noinput"
     args:
      chdir: "{{nm2_root}}"
   - name: update locales
     command: "{{nm2_root}}/manage.py compilemessages"
     args:
      chdir: "{{nm2_root}}"

   - name: fix group permissions
     file:
      path: "{{nm2_root}}"
      state: directory
      recurse: yes
      group: nm

   - name: fix user permissions
     file:
      path: "{{nm2_root}}/data"
      state: directory
      recurse: yes
      owner: nm-web
      mode: g+ws
